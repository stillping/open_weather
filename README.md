# London Open Weather
The application shows the current weather for London.

### Running the application
In order to compile the application, an Open Weather api key has to be added to the project. Create a new file on the root folder named 'apikey.properties’. Inside the file add the following line:
> open_weather_api_key="###"

Swap the hashes with the actual Open Weather key.

### Running the test
* __Unit tests__ don’t require any settings
* __Instrumented tests__ need api request call to be mapped locally to the file provided under the "canned_responses" folder.

### Further development 

* Implement logic to check connectivity in order to make server request only if connection is available and to update user of internet issues
* Persist data locally with the use of a Room database
* Implement rate limit rules so that the persisted data can be used in the event that the server refresh period has not passed.
* Implement logic so that the user can be updated of errors from server requests
* Implement WorkManager so that forecast data is refreshed every 10min (server refresh period)
* Add menu item so that the user can refresh the forecast data.
* Add menu item so that the user can select the temperature units. Persist selection as Preference

