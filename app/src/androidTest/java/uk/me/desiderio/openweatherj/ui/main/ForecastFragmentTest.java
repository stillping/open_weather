package uk.me.desiderio.openweatherj.ui.main;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.me.desiderio.openweatherj.MainActivity;
import uk.me.desiderio.openweatherj.R;
import uk.me.desiderio.openweatherj.SimpleCountingIdlingResource;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * test for the {@link ForecastFragment}
 */
@RunWith(AndroidJUnit4ClassRunner.class)
public class ForecastFragmentTest {

    @Rule
    public final ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        IdlingRegistry.getInstance().register(SimpleCountingIdlingResource.getIdlingResource());
    }

    @Test
    public void showsCorrectValuesInTextViews() {
        onView(withId(R.id.forecast_city_name_tf)).check(matches(withText("London")));

        onView(withId(R.id.forecast_temp_min_tf)).check(matches(withText("290.2˚C")));
        onView(withId(R.id.forecast_temp_tf)).check(matches(withText("290.5˚C")));
        onView(withId(R.id.forecast_temp_max_tf)).check(matches(withText("290.5˚C")));

        onView(withId(R.id.humidity_label)).check(matches(withText(R.string.forecast_humidity_label)));
        onView(withId(R.id.forecast_humidity_tf)).check(matches(withText("64")));
        onView(withId(R.id.preassure_label)).check(matches(withText(R.string.forecast_preassure)));
        onView(withId(R.id.forecast_preassure_tf)).check(matches(withText("1014.15")));

        // TODO: implement matcher to check right image is loaded
        onView(withId(R.id.forecast_condition_icon_iv)).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() {
        IdlingRegistry.getInstance().unregister(SimpleCountingIdlingResource.getIdlingResource());
    }


}