package uk.me.desiderio.openweatherj;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import uk.me.desiderio.openweatherj.di.AppModule;
import uk.me.desiderio.openweatherj.di.DaggerAppComponent;

public class OpenWeatherApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }
}
