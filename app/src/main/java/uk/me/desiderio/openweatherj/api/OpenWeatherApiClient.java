package uk.me.desiderio.openweatherj.api;

import javax.inject.Inject;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OpenWeatherApiClient {
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    private final Retrofit retrofit;

    @Inject
    public OpenWeatherApiClient() {
        this.retrofit = getRetrofit();
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public WeatherForecastService getWeatherForecastService() {
        return retrofit.create(WeatherForecastService.class);
    }
}
