package uk.me.desiderio.openweatherj.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import uk.me.desiderio.openweatherj.api.vo.Forecast;

public interface WeatherForecastService {

    // http://api.openweathermap.org/data/2.5/forecast?APPID=1111&id=2643743&units=metric&cnt=1
    @GET("forecast")
    public Call<Forecast> getWeatherForLocationById(
            @Query("APPID") String appId,
            @Query("id") int id,
            @Query("units") String units,
            @Query("cnt") int cnt);
}
