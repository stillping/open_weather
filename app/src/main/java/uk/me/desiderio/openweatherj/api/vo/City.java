package uk.me.desiderio.openweatherj.api.vo;

public class City {
    public String country;
    public int id;
    public String name;
    public int population;
    public int timezone;
}
