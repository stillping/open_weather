package uk.me.desiderio.openweatherj.api.vo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Forecast {
    public String cod;
    public int cnt;
    public City city;

    @SerializedName("list")
    public List<ListItem> forecastList;

}
