package uk.me.desiderio.openweatherj.api.vo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListItem {
    public long dt;
    public String dt_txt;
    public MainInfo main;

    @SerializedName("weather")
    public List<Weather> weatherList;

    public Wind wind;
    public Snow snow;
    public Rain rain;
    public Clouds clouds;

}
