package uk.me.desiderio.openweatherj.api.vo;

import com.google.gson.annotations.SerializedName;

public class MainInfo {
    @SerializedName("grnd_level")
    public float groundLevel;
    public int humidity;
    public float pressure;

    @SerializedName("sea_level")
    public float seaLevel;
    public float temp;
    public float temp_kf;

    @SerializedName("temp_max")
    public float temperatureMax;

    @SerializedName("temp_min")
    public float temperatureMin;
}
