package uk.me.desiderio.openweatherj.api.vo;

import com.google.gson.annotations.SerializedName;

public class Precipitation {

    @SerializedName("3h")
    public int precipitationInMilims;
}
