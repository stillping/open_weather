package uk.me.desiderio.openweatherj.api.vo;

public class Weather {

    public int id;
    public String description;
    public String icon;
    public String main;
}
