package uk.me.desiderio.openweatherj.api.vo;

class Wind {
    public float deg;
    public float speed;
}
