package uk.me.desiderio.openweatherj.data;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.me.desiderio.openweatherj.BuildConfig;
import uk.me.desiderio.openweatherj.api.OpenWeatherApiClient;
import uk.me.desiderio.openweatherj.api.vo.Forecast;

class NetworkDataSource {
    private static final String TAG = NetworkDataSource.class.getSimpleName();


    private MutableLiveData<Forecast> forecastLiveData;

    private OpenWeatherApiClient apiClient;

    @Inject
    public NetworkDataSource(OpenWeatherApiClient apiClient) {
        this.apiClient = apiClient;

        forecastLiveData = new MutableLiveData<>();
    }

    public LiveData<Forecast> getForecastLiveData() {
        return forecastLiveData;
    }

    public void requestNetworkForecastData(int locId, TempUnit tempUnit) {
        getForecastCall(locId, tempUnit).enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                if (response.body() != null) {
                    forecastLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private Call<Forecast> getForecastCall(int locId, TempUnit tempUnit) {
        return apiClient.getWeatherForecastService()
                .getWeatherForLocationById(BuildConfig.OPEN_WEATHER_API_KEY,
                                           locId,
                                           tempUnit.getUnitName(),
                                           1);
    }
}
