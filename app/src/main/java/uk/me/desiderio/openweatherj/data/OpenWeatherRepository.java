package uk.me.desiderio.openweatherj.data;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import uk.me.desiderio.openweatherj.api.vo.Forecast;
import uk.me.desiderio.openweatherj.api.vo.ListItem;
import uk.me.desiderio.openweatherj.api.vo.Weather;
import uk.me.desiderio.openweatherj.ui.vo.ForecastViewData;
import uk.me.desiderio.openweatherj.util.AppExecutors;

/**
 * provides interface for the data layer to make data accesible to the rest of application
 */
@Singleton
public class OpenWeatherRepository {

    private static final int LONDON_LOC_ID = 2643743;
    private static final TempUnit TEMP_UNIT = TempUnit.METRIC;

    private final NetworkDataSource dataSource;

    private final MutableLiveData<ForecastViewData> liveData;

    @Inject
    public OpenWeatherRepository(NetworkDataSource dataSource,
                                 final AppExecutors appExecutors) {
        this.dataSource = dataSource;
        this.liveData = new MutableLiveData<>();

        dataSource.getForecastLiveData()
                .observeForever(forecast -> appExecutors.getDiskIO().execute(() -> {
                    liveData.postValue(getViewData(forecast));
                }));
    }

    public LiveData<ForecastViewData> getLiveData() {
        return liveData;
    }

    public void requestNetworkForecastData() {
        dataSource.requestNetworkForecastData(LONDON_LOC_ID, TEMP_UNIT);
    }

    private ForecastViewData getViewData(Forecast forecast) {
        ListItem listItem = forecast.forecastList.get(0);
        Weather weather = listItem.weatherList.get(0);
        return new ForecastViewData(forecast.city.name,
                                    weather.description,
                                    weather.main,
                                    weather.icon,
                                    listItem.main.humidity,
                                    listItem.main.pressure,
                                    listItem.main.temp,
                                    listItem.main.temperatureMax,
                                    listItem.main.temperatureMin,
                                    TEMP_UNIT.getUnitName());
    }
}