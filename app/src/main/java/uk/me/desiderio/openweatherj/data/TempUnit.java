package uk.me.desiderio.openweatherj.data;

public enum TempUnit {

    METRIC("metric"),
    KELVIN(""),
    IMPERIAL("imperial");

    private String unitName;

    TempUnit(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
}
