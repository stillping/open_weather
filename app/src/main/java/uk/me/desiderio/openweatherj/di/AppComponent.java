package uk.me.desiderio.openweatherj.di;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import uk.me.desiderio.openweatherj.OpenWeatherApplication;
import uk.me.desiderio.openweatherj.di.binding.ActivityBindingModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBindingModule.class})
public interface AppComponent extends AndroidInjector<OpenWeatherApplication> {

    @Component.Builder
    interface Builder {
        Builder appModule(AppModule appModule);

        AppComponent build();
    }
}
