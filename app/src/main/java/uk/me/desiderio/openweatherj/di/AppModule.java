package uk.me.desiderio.openweatherj.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.me.desiderio.openweatherj.di.viewmodel.ViewModelModule;
import uk.me.desiderio.openweatherj.util.AppExecutors;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    private final Application app;

    public AppModule(Application app) {
        this.app = app;
    }

    @Singleton
    @Provides
    Context providesContext() {
        return app;
    }

    @Singleton
    @Provides
    AppExecutors providesAppExecutors() {
        return AppExecutors.getInstance();
    }
}
