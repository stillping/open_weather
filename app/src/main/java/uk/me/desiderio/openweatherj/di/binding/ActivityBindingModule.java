package uk.me.desiderio.openweatherj.di.binding;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import uk.me.desiderio.openweatherj.MainActivity;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {
            ForecastFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();

}
