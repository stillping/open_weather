package uk.me.desiderio.openweatherj.di.binding;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import uk.me.desiderio.openweatherj.ui.main.ForecastFragment;

@Module(subcomponents = ForecastFragmentSubcomponent.class)
public abstract class ForecastFragmentBindingModule {


    private ForecastFragmentBindingModule() {
    }

    @Binds
    @IntoMap
    @ClassKey(ForecastFragment.class)
    abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
            ForecastFragmentSubcomponent.Factory factory);
}



