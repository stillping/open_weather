package uk.me.desiderio.openweatherj.di.binding;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.me.desiderio.openweatherj.ui.main.ForecastFragment;

@Subcomponent
public interface ForecastFragmentSubcomponent extends AndroidInjector<ForecastFragment> {

    // TODO look into deprecated builder
    @Subcomponent.Factory
    public interface Factory extends AndroidInjector.Factory<ForecastFragment> {
    }

}
