package uk.me.desiderio.openweatherj.di.viewmodel;

import androidx.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import uk.me.desiderio.openweatherj.ui.main.ForecastFragmentViewModel;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ForecastFragmentViewModel.class)
    abstract ViewModel providesListItemsViewModel(ForecastFragmentViewModel listItemsViewModel);
}
