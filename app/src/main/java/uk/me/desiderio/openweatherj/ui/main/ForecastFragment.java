package uk.me.desiderio.openweatherj.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import uk.me.desiderio.openweatherj.R;
import uk.me.desiderio.openweatherj.SimpleCountingIdlingResource;
import uk.me.desiderio.openweatherj.di.viewmodel.ViewModelFactory;
import uk.me.desiderio.openweatherj.ui.vo.ForecastViewData;
import uk.me.desiderio.openweatherj.util.ImageLoader;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    ImageLoader imageLoader;

    private TextView cityNameTextView;
    private TextView conditionDescTextView;
    private TextView tempTextView;
    private TextView tempMaxTextView;
    private TextView tempMinTextView;
    private TextView humidityTextView;
    private TextView pressureTextView;
    private ImageView conditionIconImageView;

    public ForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forecast, container, false);

        cityNameTextView = rootView.findViewById(R.id.forecast_city_name_tf);
        conditionDescTextView = rootView.findViewById(R.id.forecast_condition_desc_tf);
        tempTextView = rootView.findViewById(R.id.forecast_temp_tf);
        tempMaxTextView = rootView.findViewById(R.id.forecast_temp_max_tf);
        tempMinTextView = rootView.findViewById(R.id.forecast_temp_min_tf);
        humidityTextView = rootView.findViewById(R.id.forecast_humidity_tf);
        pressureTextView = rootView.findViewById(R.id.forecast_preassure_tf);
        conditionIconImageView = rootView.findViewById(R.id.forecast_condition_icon_iv);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ForecastFragmentViewModel viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(ForecastFragmentViewModel.class);

        SimpleCountingIdlingResource.increment();
        viewModel.getForecastData().observe(this, viewData -> {
            updateView(viewData);
            SimpleCountingIdlingResource.decrement();
        });

        if (savedInstanceState == null) {
            viewModel.requestNetworkForecastData();
        }
    }

    private void updateView(ForecastViewData viewData) {
        cityNameTextView.setText(viewData.cityName);
        conditionDescTextView.setText(viewData.currentWeatherDesc);
        tempTextView.setText(getTemperatureString(viewData.temperature, viewData.tempUnit));
        tempMaxTextView.setText(getTemperatureString(viewData.maxTemperature, viewData.tempUnit));
        tempMinTextView.setText(getTemperatureString(viewData.minTemperature, viewData.tempUnit));
        humidityTextView.setText(String.valueOf(viewData.humidity));
        pressureTextView.setText(String.valueOf(viewData.preassure));

        imageLoader.getWeatherConditionDrawable(viewData.currentWeatherIcon, conditionIconImageView);
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    private String getTemperatureString(float temperature, String unit) {
        if (unit.equals("metric")) {
            return String.format("%.1f˚C", temperature);
        }
        return "";
    }
}
