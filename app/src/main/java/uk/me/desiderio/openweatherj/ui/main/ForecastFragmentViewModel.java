package uk.me.desiderio.openweatherj.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import uk.me.desiderio.openweatherj.data.OpenWeatherRepository;
import uk.me.desiderio.openweatherj.ui.vo.ForecastViewData;

public class ForecastFragmentViewModel extends ViewModel {

    private OpenWeatherRepository repository;

    @Inject
    public ForecastFragmentViewModel(OpenWeatherRepository repository) {
        this.repository = repository;
    }

    public LiveData<ForecastViewData> getForecastData() {
        return repository.getLiveData();
    }

    public void requestNetworkForecastData() {
        repository.requestNetworkForecastData();
    }
}
