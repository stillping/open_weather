package uk.me.desiderio.openweatherj.ui.vo;

/**
 * Data Object for data to be displayed on ui
 */
public class ForecastViewData {
    public String cityName;

    public String currentWeatherDesc;
    public String currentWeatherName;
    public String currentWeatherIcon;

    public int humidity;
    public float preassure;
    public float temperature;
    public float maxTemperature;
    public float minTemperature;

    public String tempUnit;


    public ForecastViewData(String cityName,
                            String currentWeatherDesc,
                            String currentWeatherName,
                            String currentWeatherIcon,
                            int humidity,
                            float preassure,
                            float temperature,
                            float maxTemperature,
                            float minTemperature,
                            String tempUnit) {
        this.cityName = cityName;
        this.currentWeatherDesc = currentWeatherDesc;
        this.currentWeatherName = currentWeatherName;
        this.currentWeatherIcon = currentWeatherIcon;
        this.humidity = humidity;
        this.preassure = preassure;
        this.temperature = temperature;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
        this.tempUnit = tempUnit;
    }
}
