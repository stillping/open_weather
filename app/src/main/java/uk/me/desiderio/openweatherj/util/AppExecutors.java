package uk.me.desiderio.openweatherj.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * global executor pool
 * as described at CodeLabs' "Build an App with Architecture Components"
 */

public class AppExecutors {

    protected static final Object LOCK = new Object();
    protected static AppExecutors instance;
    private Executor diskIO;
    private Executor networkIO;


    protected AppExecutors(Executor diskIO, Executor networkIO) {
        this.diskIO = diskIO;
        this.networkIO = networkIO;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static AppExecutors getInstance() {
        if (instance == null) {
            synchronized (LOCK) {
                instance = new AppExecutors(Executors.newSingleThreadExecutor(),
                                            Executors.newFixedThreadPool(3));
            }
        }
        return instance;
    }

    public Executor getDiskIO() {
        return diskIO;
    }

    public Executor getNetworkIO() {
        return networkIO;
    }
}
