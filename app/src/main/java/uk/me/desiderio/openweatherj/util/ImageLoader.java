package uk.me.desiderio.openweatherj.util;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class ImageLoader {
    private static final String IMAGE_BASE_URL_TEMPLATE = "http://openweathermap.org/img/wn/%s@2x" +
            ".png";

    private Context context;

    @Inject
    public ImageLoader(Context context) {
        this.context = context;
    }

    public void getWeatherConditionDrawable(String imageQuery,
                                                   ImageView imageView) {
        String url = String.format(IMAGE_BASE_URL_TEMPLATE, imageQuery);
        Picasso.with(context).load(url).into(imageView);
    }
}
