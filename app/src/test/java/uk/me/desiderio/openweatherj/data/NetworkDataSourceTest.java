package uk.me.desiderio.openweatherj.data;

import androidx.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.MockitoJUnitRunner;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.me.desiderio.openweatherj.api.OpenWeatherApiClient;
import uk.me.desiderio.openweatherj.api.WeatherForecastService;
import uk.me.desiderio.openweatherj.api.vo.Forecast;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link NetworkDataSource}
 */
@RunWith(MockitoJUnitRunner.class)
public class NetworkDataSourceTest {

    private NetworkDataSource dataSource;

    @Mock
    private OpenWeatherApiClient apiClient;
    @Mock
    private WeatherForecastService forecastService;
    @Mock
    private Call<Forecast> forecastCall;
    @Mock
    private MutableLiveData<Forecast> mockedForecastMutableLiveData;

    @Before
    public void setUp() {
        when(apiClient.getWeatherForecastService()).thenReturn(forecastService);

        dataSource = new NetworkDataSource(apiClient);
    }

    @Test
    public void givenApiSuccessResponse_whenRequestItemList_thenLivedataPostValue() throws NoSuchFieldException {
        //given
        when(forecastService.getWeatherForLocationById(any(String.class),
                                                       any(Integer.class),
                                                       any(String.class),
                                                       any(Integer.class)))
                .thenReturn(forecastCall);

        FieldSetter.setField(dataSource,
                             dataSource.getClass().getDeclaredField("forecastLiveData"),
                             mockedForecastMutableLiveData);

        Forecast expectedForecast = new Forecast();


        doAnswer(invocation -> {
            Callback<Forecast> callback = (Callback) invocation.getArguments()[0];
            callback.onResponse(forecastCall, Response.success(expectedForecast));
            return null;
        }).when(forecastCall).enqueue(any(Callback.class));

        // when
        dataSource.requestNetworkForecastData(222, TempUnit.METRIC);

        // then
        verify(mockedForecastMutableLiveData, times(1)).setValue(expectedForecast);
    }
}