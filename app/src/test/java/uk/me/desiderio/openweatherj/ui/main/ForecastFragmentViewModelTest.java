package uk.me.desiderio.openweatherj.ui.main;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import uk.me.desiderio.openweatherj.data.OpenWeatherRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/** test for {@link ForecastFragmentViewModel} */

@RunWith(MockitoJUnitRunner.class)
public class ForecastFragmentViewModelTest {

    @Mock
    private OpenWeatherRepository repository;

    private ForecastFragmentViewModel viewModel;

    @Before
    public void setUp() throws Exception {
        viewModel = new ForecastFragmentViewModel(repository);
    }

    @Test
    public void whenNetworkdataIsRequested_thenRequestIsDelegatedToRepository() {
        viewModel.requestNetworkForecastData();

        verify(repository, times(1)).requestNetworkForecastData();
    }

    @Test
    public void whenLiveDataIsRequested_thenReturnRepositoryLiveData() {
        viewModel.getForecastData();

        verify(repository, times(1)).getLiveData();
    }
}